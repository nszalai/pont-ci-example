package eu.pnt.app.controller;

import eu.pnt.app.entity.Human;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by nszalai on 2014.03.25..
 */
public class DataListControllerTest {

    private DataListController controller;

    @Before
    public void method() {
        controller = new DataListController();
        controller.createInitData();
    }

    @Test
    public void testCreateInitData() {
        Assert.assertNotNull(controller.getList());
    }

    @Test
    public void testHeaderText() {
        Assert.assertNotNull(controller.getHeaderText());
    }

    @Test
    public void testHumanEntity() {
        List<Human> list = controller.getList();
        Assert.assertNotNull(list.get(0));
    }

}

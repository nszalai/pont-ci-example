package eu.pnt.app.entity;

/**
 * Created by nszalai on 2014.03.25..
 */
public class Human {

    private int id;

    private String firstName;
    private String lastName;
    private boolean active;

    public Human(final String fn, final String ln, final boolean b) {
        firstName = fn;
        lastName = ln;
        active = b;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

package eu.pnt.app.controller;

/**
 * Created by nszalai on 2014.03.25..
 */

import eu.pnt.app.entity.Human;
import org.apache.commons.collections.CollectionUtils;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "dataListController")
@ApplicationScoped
public class DataListController {

    private String headerText = "Hello World! :) ";
    private List<Human> list = new ArrayList<Human>();

    public void createInitData() {
        if (CollectionUtils.isEmpty(list)) {
            list.add(new Human("Teszte", "Elek", true));
            list.add(new Human("Tesztv", "Virag", false));
        }
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public List<Human> getList() {
        return list;
    }

    public void setList(List<Human> list) {
        this.list = list;
    }
}
